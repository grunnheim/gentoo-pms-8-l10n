\chapter{ebuild 定义的变量}
\label{ch:ebuild-vars}

\note{本章描述的是 ebuild 可选或必须定义的变量。关于软件包管理器传递给 ebuild
的变量，请看~\ref{sec:ebuild-env-vars} 节。}

如果这些变量中的任何一个设为了无效值，或者某个必须定义的变量没有定义，遇到这种情况软件包
管理器的行为不做定义；理想情况下，一个 ebuild 的错误不应该阻碍对其他 ebuild 或软件包的操作。

\section{元数据不变性}
\label{sec:metadata-invariance}

本章讨论的所有 ebuild 定义的变量都必须独立于系统、系统轮廓以及目录树相关的数据进行定义，
并且不能随 ebuild 阶段而变化。具体而言，ebuild 元数据会不在 ebuild 将要使用的系统上生成，
并且 ebuild 必须每次使用都生成完全相同的元数据。

没有特殊含义的全局定义的 ebuild 变量同样不能依赖可变数据。

\section{ebuild 必须定义的变量}
\label{sec:mandatory-vars}

所有 ebuild 至少要定义以下变量：
\nobreakpar
\begin{description}
\item[DESCRIPTION] 一段给人看的对软件包目的的简短描述。可以由 eclass 定义。不能为空。
\item[SLOT] 软件包的插槽。根据~\ref{sec:slot-names} 节，必须是一个有效的插槽名称。
    可以由 eclass 定义。不能为空。

    在表~\ref{tab:slot-deps-table} 列出的支持子插槽的 EAPI 中，\t{SLOT} 变量可以包含一个
    可选的子插槽部分，写在普通插槽的后面，以 \t{/} 字符分隔。根据~\ref{sec:slot-names} 节，
    子插槽必须是一个有效的插槽名称。子插槽用来表示把一个软件包升级到子插槽不同的新版本可能
    需要重新构建依赖它的软件包的情况。如果在 \t{SLOT} 定义中省略了子插槽部分，软件包就被
    视为具有和普通插槽相等的隐式子插槽。
\end{description}

\section{ebuild 可选定义的变量}
\label{sec:optional-vars}

ebuild 可以定义以下变量：
\nobreakpar
\begin{description}
\item[EAPI] EAPI。详见下文~\ref{sec:eapi} 节。
\item[HOMEPAGE] 软件包主页的一个或多个 URI，包含协议。完整的语法见~\ref{sec:dependency-spec} 节。
\item[SRC_URI] 软件包源码 URI 的列表。有效的协议有 \t{http://}、\t{https://}、\t{ftp://} 和
    \t{mirror://}（mirror 协议的行为见~\ref{sec:thirdpartymirrors} 节）。那些拉取受限的软件包
    可以添加仅由一个文件名组成的 URL。完整的语法见~\ref{sec:dependency-spec} 节，详细描述见
    \ref{sec:src-uri-behaviour} 节。
\item[LICENSE] 软件包的许可证。根据~\ref{sec:license-names} 节，每一个文本标记必须是一个有效的
    许可证名称，并且必须对应于“licenses/”目录下的一个文件（详见~\ref{sec:licenses-dir} 节）。
    完整的语法见~\ref{sec:dependency-spec} 节。
\item[KEYWORDS] 以空格分隔的 ebuild 关键字列表。根据~\ref{sec:keyword-names} 节，每一个标记必须
    是一个有效的关键字名称。完整的语法见~\ref{sec:keywords} 节。
\item[IUSE] ebuild 使用的 \t{USE} 标志。任何使用 \t{USE} 标志的 eclass 也必须设置 \t{IUSE}，
    只列出该 eclass 使用的变量。软件包管理器负责合并这些值。关于此变量中必须列出哪些值的讨论见
    \ref{sec:use-iuse-handling} 节。

    \featurelabel{iuse-defaults} 在表~\ref{tab:iuse-defaults-table} 列出的支持 \t{IUSE} 默认值的
    EAPI 中，\t{IUSE} 里的 use 标志名称前面可以加一个加号或者减号前缀。如果加了这种前缀，在没有
    其他配置覆盖的前提下软件包管理器可以把它用作关于 use 标志默认值的建议。
\item[REQUIRED_USE] \featurelabel{required-use} 任意数量个断言，这些断言必须由 \t{USE}
    标志的组合满足才能对 ebuild 生效。完整的语法见~\ref{sec:dependency-spec} 节，详细描述见
    \ref{sec:required-use} 节。仅适用于表~\ref{tab:optional-vars-table} 列出的支持
    \t{REQUIRED_USE} 的 EAPI。
\item[PROPERTIES] \featurelabel{properties} 该软件包的任意数量个属性。完整的语法见
    \ref{sec:dependency-spec} 节，值的含义见~\ref{sec:properties} 节。对于表
    \ref{tab:optional-vars-table} 列出的对此变量只有可选支持的 EAPI，ebuild
    不得以任何形式依赖软件包管理器能够识别或理解此变量。
\item[RESTRICT] 该软件包的任意数量个行为约束。完整的语法见~\ref{sec:dependency-spec} 节，
    值的含义见~\ref{sec:restrict} 节。
\item[DEPEND] 见第\ref{ch:dependencies}章。
\item[BDEPEND] 见第\ref{ch:dependencies}章。
\item[RDEPEND] 见第\ref{ch:dependencies}章。对于某些 EAPI，\t{RDEPEND} 的值如果没有设置或是在
    eclass 中设置会产生特殊行为。相关细节见~\ref{sec:rdepend-depend} 节。
\item[PDEPEND] 见第\ref{ch:dependencies}章。
\item[IDEPEND] 见第\ref{ch:dependencies}章。
\end{description}

\ChangeWhenAddingAnEAPI{8}
\begin{centertable}{支持 \t{IUSE} 默认值的 EAPI}
    \label{tab:iuse-defaults-table}
    \begin{tabular}{ll}
      \toprule
      \multicolumn{1}{c}{\textbf{EAPI}} &
      \multicolumn{1}{c}{\textbf{支持 \t{IUSE} 默认值？}} \\
      \midrule
      0                      & 否 \\
      1, 2, 3, 4, 5, 6, 7, 8 & 是 \\
      \bottomrule
    \end{tabular}
\end{centertable}

\ChangeWhenAddingAnEAPI{8}
\begin{centertable}{支持各种 ebuild 定义的变量的 EAPI}
    \label{tab:optional-vars-table}
    \begin{tabular}{lll}
      \toprule
      \multicolumn{1}{c}{\textbf{EAPI}} &
      \multicolumn{1}{c}{\textbf{支持 \t{PROPERTIES}？}} &
      \multicolumn{1}{c}{\textbf{支持 \t{REQUIRED_USE}？}} \\
      \midrule
      0, 1, 2, 3        & 可选 & 否 \\
      4, 5, 6, 7, 8     & 是   & 是 \\
      \bottomrule
    \end{tabular}
\end{centertable}

\subsection{EAPI}
\label{sec:eapi}

\t{EAPI} 设为空值或不设相当于 \t{0}。如果 ebuild 期望这两个值之一，
则不得假设自己会得到这两个值当中特定的一个。

在生成元数据时，软件包管理器要么把 \t{EAPI} 变量预先设为 \t{0}，要么就确保变量在 ebuild
拉入之前未设置。当 ebuild 用于别的用途时，软件包管理器要么把 \t{EAPI} 变量预先设为 ebuild
元数据指定的值，要么就确保变量未设置。

如果这些变量中的任何一个设为了无效值，软件包管理器的行为不做定义；理想情况下，一个 ebuild
的错误不应该阻碍对其他 ebuild 或软件包的操作。

如果要在 ebuild 中指定 EAPI，那么 \t{EAPI} 变量必须有且仅有一次赋值。除了空白行和以可选的
空白字符（空格或制表符）加 \t{\#} 字符开头的行以外，任何行都不能放在赋值语句之前，并且
赋值语句这一行必须匹配如下正则表达式：
\begin{verbatim}
^[ \t]*EAPI=(['"]?)([A-Za-z0-9+_.-]*)\1[ \t]*([ \t]#.*)?$
\end{verbatim}

软件包管理器必须用上面的正则表达式解析 ebuild 的第一个非空白且非注释的行来确定 EAPI。
如果正则表达式匹配，则 EAPI 是捕获括号匹配到的子字符串（空字符串视为 \t{0}），否则就是
\t{0}。对于识别出的 EAPI，软件包管理器必须确认通过 bash 拉入 ebuild 得到的 \t{EAPI}
值和通过解析得到的 EAPI 相同。如果这两个值不相同则该 ebuild 必须当作无效的对待。

\subsection{SRC_URI}
\label{sec:src-uri-behaviour}

\t{SRC_URI} 中所有已启用（即不在不匹配的 use 条件组中）的 URI 下载得到的文件都必须在
\t{DISTDIR} 目录中可用。另外，这些文件名还用于生成 \t{A} 和 \t{AA} 变量。

如果一个元素是一个带协议的完整 URI，则优先使用该下载位置。软件包管理器也可以在
镜像站中查找需要的文件。

必须支持特殊的 \t{mirror://} 协议。协议的细节详见~\ref{sec:thirdpartymirrors} 节。

\t{RESTRICT} 元数据键可以用来对下载施加额外的限制——详见~\ref{sec:restrict} 节。
拉取受限的软件包可以用一个简单的文件名代替完整的 URI。

\featurelabel{src-uri-arrows} 在表~\ref{tab:uri-arrows-table} 列出的支持箭头的 EAPI 中，
如果使用了箭头，那么保存到 \t{DISTDIR} 的文件名应改为箭头右侧的文件名。当在镜像站中查找
文件时（除了箭头左侧 URI 使用的是 \t{mirror://} 协议的情况之外），应该请求箭头右侧的
文件名而不是 URI 中的文件名。

\featurelabel{uri-restrict} 在表~\ref{tab:uri-arrows-table} 列出的支持选择性 URI 约束的
EAPI 中，URI 协议的前边可以附加一个 \t{fetch+} 或 \t{mirror+} 前缀。如果 ebuild 拉取受限，
\t{fetch+} 前缀撤销 URI 的拉取约束（但不会影响隐含的镜像约束）。如果 ebuild 有拉取或
镜像约束，\t{mirror+} 前缀同时撤销 URI 的这两项约束。

\ChangeWhenAddingAnEAPI{8}
\begin{centertable}{支持 \t{SRC_URI} 箭头与选择性 URI 约束的 EAPI}
    \label{tab:uri-arrows-table}
    \begin{tabular}{lll}
      \toprule
      \multicolumn{1}{c}{\textbf{EAPI}} &
      \multicolumn{1}{c}{\textbf{支持 \t{SRC_URI} 箭头？}} &
      \multicolumn{1}{c}{\textbf{支持选择性 URI 约束？}} \\
      \midrule
      0, 1              & 否 & 否 \\
      2, 3, 4, 5, 6, 7  & 是 & 否 \\
      8                 & 是 & 是 \\
      \bottomrule
    \end{tabular}
\end{centertable}

\subsection{关键字}
\label{sec:keywords}

关键字用来表示软件包在架构 \t{arch} 上的稳定性等级。采用以下约定：
\begin{compactitem}
\item \t{arch}：软件包版本和 ebuild 经过全面测试，已知在此平台上工作正常并且没有
    任何严重的问题。这称为\i{稳定关键字}。
\item \t{\textasciitilde arch}：一般认为软件包版本和 ebuild 可以工作并且没有任何
    已知的严重 bug，但在认定软件包版本适合获得稳定关键字之前还需要进一步测试。这
    称为\i{不稳定关键字}或\i{测试中关键字}。
\item 无关键字：不知道软件包是否可以工作，或者只进行了不充分的测试。
\item \t{-arch}：软件包版本在此架构上不能工作。
\end{compactitem}
\t{-*} 关键字用来表示软件包版本在未列出的架构上没有尝试测试的价值。

空的 \t{KEYWORDS} 变量表示在每个架构上是否能用都不确定。

\subsection{USE 状态约束}
\label{sec:required-use}

\t{REQUIRED_USE} 包含了一个断言列表，\t{USE} 标志的组合必须满足其中的断言才能对 ebuild 生效。
为了实现匹配，终端元素中的 \t{USE} 标志必须启用（如果有感叹号前缀的话则是禁用）。

如果软件包管理器遇到了一个 \t{REQUIRED_USE} 断言不满足的软件包版本，那么这个软件包版本
就要当成已屏蔽对待，不得调用任何阶段函数。

使用不在 \t{IUSE_EFFECTIVE} 中的标志会产生一个错误。

\subsection{属性}
\label{sec:properties}

下列标记允许出现在 \t{PROPERTIES} 中：
\nobreakpar
\begin{description}
\item[interactive] 软件包可能需要通过 tty 和用户交互。
\item[live] 软件包使用的是每次安装时都可能有所不同的“实时”源码。
\item[test_network] 软件包管理器可以执行需要互联网连接的测试，即使
    ebuild 有 \t{RESTRICT=test}。
\end{description}

软件包管理器可以辨认其他的标记。ebuild 不得依赖软件包管理器对任何标记的支持。

\subsection{行为约束}
\label{sec:restrict}

下列标记允许出现在 \t{RESTRICT} 中：
\nobreakpar
\begin{description}
\item[mirror] 软件包的 \t{SRC_URI} 条目不在镜像站中，当拉取时不应该检查镜像站。
\item[fetch] 软件包的 \t{SRC_URI} 条目不会自动下载。如果条目不可用，就调用
    \t{pkg_nofetch} 函数。隐含 \t{mirror}。
\item[strip] 不对将要安装的文件执行剔除调试符号的操作。在表~\ref{tab:staging-area-commands}
    列出的支持可控剔除的 EAPI 中，此行为可以被 \t{dostrip} 命令更改。
\item[userpriv] 软件包管理器在构建软件包时不会放弃 root 权限。
\item[test] 不执行 \t{src_test} 阶段。
\end{description}

软件包管理器可以辨认其他的标记，但 ebuild 不得依赖这种支持。

\subsection{RDEPEND 值}
\label{sec:rdepend-depend}

\featurelabel{rdepend-depend} 在表~\ref{tab:rdepend-depend-table} 列出的 \t{RDEPEND=DEPEND}
的 EAPI 中，如果在 ebuild 中没有设置 \t{RDEPEND}（不同于设置为空字符串），在生成元数据时
软件包管理器必须把它的值当作等于 \t{DEPEND} 的值对待。

当处理 eclass 时，只有 ebuild 本身设置的值才会考虑此行为；eclass 中设置的 \t{DEPEND} 和
\t{RDEPEND} 都不会改变 ebuild 部分隐式的 \t{RDEPEND=DEPEND}，并且 eclass 中设置的
\t{DEPEND} 值不会成为 \t{RDEPEND} 的一部分。

\ChangeWhenAddingAnEAPI{8}
\begin{centertable}{默认情况下 \t{RDEPEND=DEPEND} 的 EAPI}
    \label{tab:rdepend-depend-table}
    \begin{tabular}{ll}
      \toprule
      \multicolumn{1}{c}{\textbf{EAPI}} &
      \multicolumn{1}{c}{\textbf{\t{RDEPEND=DEPEND}?}} \\
      \midrule
      0, 1, 2, 3        & 是 \\
      4, 5, 6, 7, 8     & 否 \\
      \bottomrule
    \end{tabular}
\end{centertable}

\section{ebuild 自动定义的变量}

下列变量必须由 \t{inherit}（详见~\ref{sec:inherit} 节）定义，并且可以视为 ebuild 元数据的一部分：

\begin{description}
\item[ECLASS] 当前的 eclass，如果当前没有 eclass 则不设。此变量由 \t{inherit}
    自动处理而不能手动修改。
\item[INHERITED] 已继承的 eclass 名单。同样由 \t{inherit} 自动处理。
\end{description}

\note{由此可见，根据对~\ref{sec:metadata-invariance} 节的延伸，\t{inherit} 不能有条件地使用，
除非是在恒定条件下。}

以下是软件包管理器为内部使用而定义的特殊变量，可以导出到 ebuild 环境中也可以不导出：

\begin{description}
\item[DEFINED_PHASES] \featurelabel{defined-phases} 一个以空格分隔，在 ebuild 或 ebuild 继承的
    eclass 中已定义的阶段函数的阶段名称（例如 \t{configure setup unpack}），以任意顺序组成的列表。
    如果没有定义阶段函数，就设为一个连字符而不是空字符串。对于表~\ref{tab:defined-phases-table}
    列出的对 \t{DEFINED_PHASES} 具有可选支持的 EAPI，软件包管理器不能指望元数据缓存里定义了这个变量，
    而且还得把空字符串视为“这项信息不可用”。
\end{description}

\note{由此可见，根据对~\ref{sec:metadata-invariance} 节的延伸，阶段函数不得基于任何可变的条件定义。}

\ChangeWhenAddingAnEAPI{8}
\begin{centertable}{各 EAPI 对 \t{DEFINED_PHASES} 的支持情况}
    \label{tab:defined-phases-table}
    \begin{tabular}{ll}
      \toprule
      \multicolumn{1}{c}{\textbf{EAPI}} &
      \multicolumn{1}{c}{\textbf{支持 \t{DEFINED_PHASES}？}} \\
      \midrule
      0, 1, 2, 3        & 可选 \\
      4, 5, 6, 7, 8     & 是   \\
      \bottomrule
    \end{tabular}
\end{centertable}

% vim: set filetype=tex fileencoding=utf8 et tw=100 spell spelllang=en :

%%% Local Variables:
%%% mode: latex
%%% TeX-master: "pms"
%%% LaTeX-indent-level: 4
%%% LaTeX-item-indent: 0
%%% TeX-brace-indent-level: 4
%%% fill-column: 100
%%% End:
