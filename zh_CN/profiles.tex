\chapter{系统轮廓}
\label{ch:profiles}

\section{一般原则}
一般来讲，系统轮廓定义了针对某一‘类型’系统的信息——它介于仓库默认值和用户配置之间，这些信息
虽然不一定适用于所有机器，但是又足够通用，所以不应该留给用户去配置它。系统轮廓的某些部分
可以被用户配置覆盖，某些部分只能被其他系统轮廓覆盖。

系统轮廓的格式相对比较简单，每个系统轮廓都是一个包含任意数量的本章所述的文件的目录，
并且可以继承其他系统轮廓。文件本身遵循些许关于继承和格式的基本惯例，这会在下一节中描述。
同时系统轮廓也可以包含任意数量个储存其他系统轮廓的子目录。

\section{构成系统轮廓的文件}

\subsection{parent 文件}
一个系统轮廓可以包含一个 \t{parent} 文件，其中的每一行是到另一个系统轮廓的相对路径，
该系统轮廓将被视为当前系统轮廓的其中一个父系统轮廓。当前系统轮廓继承并且可以覆盖
父系统轮廓的所有设置。各项设置和父系统轮廓结合的具体规则各个文件多有不同，将在下文
分别描述。父系统轮廓以深度优先，从左到右的顺序处理，每次遇到重复的父路径都会引入。

系统轮廓的继承树中包含循环是不合法的。软件包管理器遇到循环时的行为不做定义。

此文件中不能有注释、空白行和续行。

\subsection{eapi 文件}
\label{sec:profile-eapi}
系统轮廓目录可以包含一个 \t{eapi} 文件，如果存在的话，该文件中必须只有一行 EAPI 名称，
它指定了在处理相关目录时使用的 EAPI；软件包管理器不得尝试使用那些目录需要一个不支持的
EAPI 来处理的系统轮廓。如果 \t{eapi} 文件不存在，则应使用 EAPI 0。EAPI 既不从 \t{parent}
文件继承也不会传给子目录。

\subsection{deprecated 文件}
如果系统轮廓包含了一个名为 \t{deprecated} 的文件，那么它将被视为已弃用。此文件的
第一行是从仓库的 \t{profiles} 目录到一个有效的系统轮廓的相对路径，该路径是建议从
当前系统轮廓升级到的系统轮廓的路径。文件的其余部分可以是任何文本，这些文本可以由
软件包管理器显示给正在使用当前系统轮廓的用户。此文件不能被继承——继承了已弃用
系统轮廓的系统轮廓\emph{未}弃用。

此文件不能包含注释或续行。

\subsection{make.defaults 文件}
\t{make.defaults} 用来定义各种环境与配置变量的默认值。这个文件的不寻常之处在于，它没有在
文件层面上与父系统轮廓结合——相反，各个变量都会如~\ref{sec:profile-variables} 节
所述的那样独立地结合或覆盖。

文件本身是一行一行的变量与值的格式，每一行都是单个的 \verb|变量="值"| 条目，
其中的值必须用双引号引起来。变量的名称必须以 \t{a-zA-Z} 开头并且只包含
\t{a-zA-Z0-9_}。bash 语法的一个小子集，作为额外语法，也允许使用，细节如下：

\begin{compactitem}
\item 等号右边 \t{\$\{foo\}} 或 \t{\$foo} 这种形式的变量经过识别会根据变量
    在当前或更早的 \t{make.defaults} 文件中已赋的值展开。
\item 通过用反斜杠转义换行，一个逻辑行可以跨越多个物理行。双引号引起来的字符串
    跨越多个物理行既可以通过简单换行也可以通过反斜杠转义换行。
\item 除了续行之外，反斜杠不允许使用。
\end{compactitem}

\subsection{简单的基于行的文件}
\label{sec:line-stacking}
这些文件都是简单的每行一项的列表，列表会按照以下方式继承：先照抄父系统轮廓的列表，然后
把当前系统轮廓的列表追加到末尾。如果某一行以连字符开头，那么在它之前内容和该行剩余内容
相同的行将会从列表中移除。空白行和以 \t{\#} 开头的行会被丢弃。

\featurelabel{profile-file-dirs} 在表~\ref{tab:profile-file-dirs} 列出的支持用目录
替代系统轮廓文件的 EAPI 中，下文提到的 \t{package.mask}、\t{package.use}、\t{use.*}
和 \t{package.use.*} 文件中的任何一个都可以是一个目录而不是一个普通文件。该目录下的文件，
只要名称不是以点开头，都会按照 POSIX 语言环境下文件名的顺序进行拼接，得到的结果会像
单个文件一样处理。而子目录则会忽略。

\ChangeWhenAddingAnEAPI{8}
\begin{centertable}{支持用目录替代系统轮廓文件的 EAPI}
    \label{tab:profile-file-dirs}
    \begin{tabular}{ll}
      \toprule
      \multicolumn{1}{c}{\textbf{EAPI}} &
      \multicolumn{1}{c}{\textbf{支持用目录替代系统轮廓文件？}} \\
      \midrule
      0, 1, 2, 3, 4, 5, 6 & 否 \\
      7, 8                & 是 \\
      \bottomrule
    \end{tabular}
\end{centertable}

\subsection{packages}
\t{packages} 文件用于定义系统轮廓的‘system 集合’。在上文所述的继承和注释规则应用之后，
它的每一行必须是以下两种形式之一：带有 \t{*} 前缀的软件包依赖说明符，表示这是组成 system
集合的一部分。单独的一个软件包依赖说明符出于历史原因也可以出现，但在计算 system 集合时应该忽略。

\subsection{packages.build}
\t{packages.build} 文件被 Gentoo 的 Catalyst 工具用来生成 stage1 压缩包，和软件包管理器的
操作没什么关系。因此它超出了此文档的范围，只是出于完整性在这里简要提及。

\subsection{package.mask}
\t{package.mask} 用于在给定的系统轮廓中阻止某些软件包的安装。文件中的每一行是一个软件包
依赖说明符；和该说明符匹配的任何软件包都不会安装，除非被用户配置解除屏蔽。根据
\ref{sec:line-stacking} 节，在有些 EAPI 中 \t{package.mask} 可以是一个目录而不是普通文件。

需要说明的是 \t{-说明符}\ 语法可以用来移除父系统轮廓中屏蔽的说明符，且不一定
非得是（\ref{sec:profiles-dir} 节，\t{profiles/package.mask} 里）全局屏蔽的说明符。

\note{目前 Portage 在涉及到 \t{-某行}\ 时会将 \t{profiles/package.mask}
视为位于继承树的最左侧分支。该行为不是此规范的一部分。}

\subsection{package.provided}
\featurelabel{package-provided} \t{package.provided} 用来告诉软件包管理器某个软件包版本
不管是否实际安装，都应该视为已经由系统提供。由于它会对基于 USE 和基于插槽的依赖关系造成
严重的不利影响，所以强烈反对使用，并且软件包管理器对它的支持必须视为完全可选的。各 EAPI
对它的支持见表~\ref{tab:package-provided}。

\ChangeWhenAddingAnEAPI{8}
\begin{centertable}{各 EAPI 对系统轮廓中 \t{package.provided} 的支持}
    \label{tab:package-provided}
    \begin{tabular}{ll}
      \toprule
      \multicolumn{1}{c}{\textbf{EAPI}} &
      \multicolumn{1}{c}{\textbf{支持 \t{package.provided}？}} \\
      \midrule
      0, 1, 2, 3, 4, 5, 6 & 可选 \\
      7, 8                & 否   \\
      \bottomrule
    \end{tabular}
\end{centertable}

\subsection{package.use}
\t{package.use} 文件可以被软件包管理器用来覆盖 \t{make.defaults} 在每个软件包的基础上指定的
默认 USE 标志。每行的格式为：首先是一个软件包依赖说明符，然后是一张以空格分隔的启用 USE
标志的列表，\t{-标志}\ 形式的 USE 标志表示应该禁用软件包的此标志。软件包依赖说明符限定为目录的
EAPI 所定义的格式。根据~\ref{sec:line-stacking} 节，在有些 EAPI 中 \t{package.use}
可以是一个目录而不是普通文件。

\subsection{USE 的屏蔽与强制启用}
\label{sec:use-masking}
本节覆盖了 \t{use.mask}、\t{use.force}、\t{use.stable.mask}、
\t{use.stable.force}、\t{package.\allowbreak use.mask}、\t{package.use.force}、\t{package.use.stable.mask}、
\t{package.use.stable.\allowbreak force} 等 8 个文件。这些文件放在一起描述是因为它们以一种
非同一般的方式交互。根据~\ref{sec:line-stacking} 节，在有些 EAPI 中这些文件可以是目录而不是普通文件。

简单来说，\t{use.mask} 和 \t{use.force} 分别用来表示在使用当前系统轮廓时，给定的 USE 标志必须
始终屏蔽和启用。\t{package.use.mask} 和 \t{package.use.force} 在每个软件包或每个版本的基础上做同样的事。

\featurelabel{stablemask}
对于表~\ref{tab:profile-stablemask} 列出的支持稳定版屏蔽的 EAPI，各个系统轮廓目录下的
\t{use.stable.mask}、\t{use.stable.force}、\t{package.use.stable.mask} 和
\t{package.use.stable.force} 也有相同的功能，只不过这些文件仅对以稳定关键字（见
\ref{sec:keywords} 节）合并的软件包生效。因此这些文件可以用来约束软件包中被视为稳定的功能。

\ChangeWhenAddingAnEAPI{8}
\begin{centertable}{各 EAPI 对针对稳定版本屏蔽/强制启用 use 标志的支持}
    \label{tab:profile-stablemask}
    \begin{tabular}{ll}
      \toprule
      \multicolumn{1}{c}{\textbf{EAPI}} &
      \multicolumn{1}{c}{\textbf{支持针对稳定版本屏蔽/强制启用 use 标志？}} \\
      \midrule
      0, 1, 2, 3, 4     & 否 \\
      5, 6, 7, 8        & 是 \\
      \bottomrule
    \end{tabular}
\end{centertable}

这 8 个文件准确的交互方式没有上面说的那么简单，决定给定软件包版本的某个标志是否屏蔽的算法
才是最好的描述，请看算法~\ref{alg:use-masking}：
\begin{algorithm}
\caption{\t{USE} 屏蔽逻辑} \label{alg:use-masking}
\begin{algorithmic}[1]
\STATE 令\ 屏蔽 = 假
\FOR{每个继承树中的系统轮廓，以深度优先}
    \IF{\t{use.mask} 中有 \i{标志}}
        \STATE 令\ 屏蔽 = 真
    \ELSIF{\t{use.mask} 中有 \i{-标志}}
        \STATE 令\ 屏蔽 = 假
    \ENDIF
    \IF{用的是稳定关键字}
        \IF{\t{use.stable.mask} 中有 \i{标志}}
            \STATE 令\ 屏蔽 = 真
        \ELSIF{\t{use.stable.mask} 中有 \i{-标志}}
            \STATE 令\ 屏蔽 = 假
        \ENDIF
    \ENDIF
    \FOR{package.use.mask 中从上到下，说明符和 $\i{软件包}$ 相匹配的每 $\i{一行}$}
        \IF{$\i{一行}$ 中有 \i{标志}}
            \STATE 令\ 屏蔽 = 真
        \ELSIF{$\i{一行}$ 中有 \i{-标志}}
            \STATE 令\ 屏蔽 = 假
        \ENDIF
    \ENDFOR
    \IF{用的是稳定关键字}
        \FOR{package.use.stable.mask 中从上到下，说明符和 $\i{软件包}$ 相匹配的每 $\i{一行}$}
            \IF{$\i{一行}$ 中有 \i{标志}}
                \STATE 令\ 屏蔽 = 真
            \ELSIF{$\i{一行}$ 中有 \i{-标志}}
                \STATE 令\ 屏蔽 = 假
            \ENDIF
        \ENDFOR
    \ENDIF
\ENDFOR
\end{algorithmic}
\end{algorithm}

如果在 \t{KEYWORDS} 中把所有稳定关键字替换为相应的带波浪号前缀的关键字（见~\ref{sec:keywords} 节）
将导致软件包的安装由于 \t{KEYWORDS} 设置而被阻止，那么会完全应用稳定版约束（算法
\ref{alg:use-masking} 中的“用的是稳定关键字”）。

\t{use.force}、\t{use.stable.force}、\t{package.use.force} 和
\t{package.use.stable.force} 的逻辑也是一样的。如果某个标志既要屏蔽又要强制启用，
则认为屏蔽的优先级更高。

\t{USE_EXPAND} 值可以用 \t{expand\hspace{0em}名称_值}\ 来强制启用或屏蔽。

软件包管理器可以把不是当前架构的 \t{ARCH} 值视为屏蔽。

\input{profile-variables.tex}

% vim: set filetype=tex fileencoding=utf8 et tw=100 spell spelllang=en :

%%% Local Variables:
%%% mode: latex
%%% TeX-master: "pms"
%%% LaTeX-indent-level: 4
%%% LaTeX-item-indent: 0
%%% TeX-brace-indent-level: 4
%%% fill-column: 100
%%% End:
